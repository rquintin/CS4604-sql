## Objective
Learn more advanced SQL queries.

## Lab

### Setup
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/runme.sh && bash ./runme.sh 2`

You should see a bunch of output that ends in something like this:

```
COMMIT;
COMMIT
create table basic_cards as select player_class, type, name, cost from cards limit 30;
SELECT 30
psql (10.1)
Type "help" for help.

postgres=#
```

A data set for the collection of cards for [Hearthstone](https://playhearthstone.com/en-us/),
the popular online card game by Blizzard. This data set is freely available from 
[Kaggle](https://www.kaggle.com/jeradrose/hearthstone-cards).

### ORDER BY

The ORDER BY keyword is used to sort the result-set in ascending or descending order.

The ORDER BY keyword sorts the records in ascending order by default. 
To sort the records in descending order, use the DESC keyword. (Ascending [ASC] is the default.)

The syntax is:

```sql
SELECT column1, column2, ...
  FROM table_name
 ORDER BY column1, column2, ... ASC|DESC;
```

**Try these queries:**

```sql
SELECT name FROM basic_cards WHERE type = 'HERO' ORDER BY name;

SELECT name FROM basic_cards WHERE type = 'HERO' ORDER BY name DESC;

SELECT cost, player_class, name FROM basic_cards ORDER BY player_class, cost DESC NULLS LAST;
```

Note on the last query we are ordering first by player_class and then by cost _descending_. 
Note also that we have specified "NULLS LAST". Null values are normally sorted first. 
By adding the "NULLS LAST" directive, we are telling SQL to put rows having a NULL value at the bottom of the list.

### Aggregate functions

There are several aggregate functions available in SQL. The exact list is dependent upon the RDBMS. But most will 
have the basic functions: min(), max(), count(), avg() and sum().

**Try these queries:**

```sql
SELECT AVG(cost) FROM basic_cards;

SELECT MAX(cost) FROM basic_cards WHERE type = 'MINION'; 
```

Note on the last query we are select the most expensive card having a type of "MINION".

How would you find the cheapest card in basic_cards?

How would you cound the total number of cards? The number of cards of type 'HERO'?

### GROUP BY

The GROUP BY statement is often used with aggregate functions (COUNT, MAX, MIN, SUM, AVG) to group the result-set by one or more columns.

The syntax is:

```sql
SELECT column_name(s)
  FROM table_name
 WHERE condition
 GROUP BY column_name(s)
 ORDER BY column_name(s);
```

To select the most expensive card of each type, you would issue a query like:

```sql
SELECT type, MAX(cost) FROM basic_cards GROUP BY type;

-- To order the results by type, add an ORDER BY clause
SELECT type, MAX(cost) FROM basic_cards GROUP BY type ORDER BY type;

-- You can also order by the aggregate function
SELECT type, MAX(cost) FROM basic_cards GROUP BY type ORDER BY MAX(cost) DESC NULLS LAST;
```

When grouping, every column in your SELECT clause that is NOT an aggregate function must be included in the GROUP BY clause.

For example, try this:

```sql
SELECT name, type, MAX(cost) FROM basic_cards GROUP BY type;
```

You should have gotten an error stating:

`ERROR:  column "basic_cards.name" must appear in the GROUP BY clause or be used in an aggregate function`

### HAVING

The HAVING clause was added to SQL because the WHERE keyword could not be used with aggregate functions.
You can think of the having clause as a where clause for aggregates.

The syntax is:

```sql
SELECT column_name(s)
FROM table_name
WHERE condition
GROUP BY column_name(s)
HAVING condition
ORDER BY column_name(s);
```

This query will select the max cost for each player class if that max cost is greater than 5.

```sql
SELECT player_class, max(cost) FROM basic_cards GROUP BY player_class HAVING max(cost) > 5 ORDER BY max(cost) DESC;
```

### Your turn

Now it's your turn to apply what you've learned. There is a larger table called **cards** in the database.
Use it to answer the questions in [Canvas](https://canvas.vt.edu/) for this lab.
