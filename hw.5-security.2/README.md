## Pen Testing
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/runme.sh && bash ./runme.sh hw.5-security.2`

You should see a bunch of output that ends in something like this:

```
IMPORTANT: Before running the 'weapons' program you must change directory using the command listed below.

cd CS4604-sql/hw.5-security.2
``

From here you will need to change directories as instructed by executing `cd CS4604-sql/hw.5-security.2`.

At this point, you can execute the program: `./weapons`
