import java.sql.*;
/**
 * Connect to Database
 */
public class ConnectionFactory {
    public static final String URL = "jdbc:postgresql://postgres/";
    public static final String USER = "postgres";
    public static final String PASS = "password";
    /**
     * Get a connection to database
     * @return Connection object
     */
    public static Connection getConnection()
    {
      try {
          Class.forName("org.postgresql.Driver");
          return DriverManager.getConnection(URL, USER, PASS);
      } catch (Exception ex) {
          throw new RuntimeException("Error connecting to the database", ex);
      }
    }
    /**
     * Test Connection
     */
    public static void main(String[] args) {
        Connection connection = ConnectionFactory.getConnection();
        System.out.println("Connection to " + URL + " successful!");
    }
}

