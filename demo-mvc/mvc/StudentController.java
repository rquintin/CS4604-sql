public class StudentController {
   private Student model;
   private StudentView view;
   private StudentDao modelDao;

   public StudentController(Student model, StudentView view){
      this.modelDao = new StudentDaoImpl();
      this.model = model;
      this.view = view;
   }

   public void setStudentName(String name){
      model.setName(name);		
      modelDao.updateStudent(model);
   }

   public String getStudentName(){
      return model.getName();		
   }

   public void setStudentRollNo(int rollNo){
      model.setRollNo(rollNo);		
      modelDao.updateStudent(model);
   }

   public int getStudentRollNo(){
      return model.getRollNo();		
   }

   public void updateView(){				
      view.printStudentDetails(model.getName(), model.getRollNo());
   }	
}
