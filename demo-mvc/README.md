## Objective
Query a database table

## Lab
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/runme.sh && bash ./runme.sh demo-mvc`

You should see a bunch of output that ends in something like this:

```
---> 93a206c3a9f8
Removing intermediate container 7b061f7c7ed0
Step 6/7 : WORKDIR /mvc
 ---> 2f1e02986910
Removing intermediate container 768c1830bef1
Step 7/7 : RUN make
 ---> Running in 83b69afda001
javac /mvc/*java
 ---> 61537142c7d0
Removing intermediate container 83b69afda001
Successfully built 61537142c7d0
Successfully tagged test:latest
/mvc #
```

From here you can run the demo app using `java MyApp`
