import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class StudentDaoImpl implements StudentDao {
   Connection db;
	
   // Constructor
   public StudentDaoImpl(){
      db = ConnectionFactory.getConnection(); 
      // Make sure the table exists in the db
      createTable();
      // Pre-populate with some test data - for demo purposes only
      Student student1 = new Student("Robert",0);
      insertStudent(db, student1);
      Student student2 = new Student("John",1);
      insertStudent(db, student2);
   }
   @Override
   public void deleteStudent(Student student) {
     String query = "delete from student where rollno = ?";
     try {
        PreparedStatement pstmt = db.prepareStatement(query);
        pstmt.setInt(1,student.getRollNo());
        pstmt.executeUpdate();
     } catch(Exception e) {
       throw new RuntimeException("Ack!", e);
     }
     System.out.println("Student: Roll No " + student.getRollNo() + ", deleted from database");
   }

   //retrive list of students from the database
   @Override
   public List<Student> getAllStudents() {
     String query = "select name, rollno from student";
     List<Student> students = new ArrayList<Student>();
     try {
       Statement stmt = db.createStatement();
       ResultSet rs = stmt.executeQuery(query);

       while (rs.next()) {
         Student student = new Student(rs.getString("NAME"), rs.getInt("ROLLNO"));
         students.add(student);
       }
     } catch(Exception e) {
       throw new RuntimeException("Ack!", e);
     }
     return students;
   }

   @Override
   public Student getStudent(int rollNo) {
     String query = "select name from student where rollno = ?";
     try {
        PreparedStatement pstmt = db.prepareStatement(query);
        pstmt.setInt(1,rollNo);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            String name = rs.getString("NAME");
						return new Student(name, rollNo);
        }
     } catch(Exception e) {
       throw new RuntimeException("Ack!", e);
     } 
     return null;
   }

   @Override
   public void updateStudent(Student student) {
     insertStudent(db, student);
     System.out.println("Student: Roll No " + student.getRollNo() + ", updated in the database");
   }

   private static void insertStudent(Connection db, Student student) {
     String query = "insert into student (name, rollno) values (?, ?)";
     try {
        PreparedStatement pstmt = db.prepareStatement(query);
        pstmt.setString(1,student.getName());
        pstmt.setInt(2, student.getRollNo());
        pstmt.executeUpdate();
     } catch(SQLException e) {
       if ("23505".equals(e.getSQLState())) {
         // record exists, so let's update it
         updateStudent(db, student);
       } else {
         throw new RuntimeException("Ack! " + e.getSQLState(), e);
       }
     }
   }

   private static void updateStudent(Connection db, Student student) {
     String query = "update student set name = ? where rollno = ?";
     try {
        PreparedStatement pstmt = db.prepareStatement(query);
        pstmt.setString(1,student.getName());
        pstmt.setInt(2, student.getRollNo());
        pstmt.executeUpdate();
     } catch(SQLException e) {
       throw new RuntimeException("Ack! " + e.getSQLState(), e);
     }
   }

   private void createTable() {
     try {
      Statement stmt = db.createStatement();
      
      String sql = "CREATE TABLE STUDENT " +
                   "(name VARCHAR(255), " +
                   " rollNo INTEGER PRIMARY KEY)";

      stmt.executeUpdate(sql);
    } catch (SQLException e) {
      // Swallow the exception
      if ("42P07".equals(e.getSQLState())) {
        // Table exists, do nothing
      } else {
        throw new RuntimeException("Error creating student table", e);
      }
    }
  }
}
