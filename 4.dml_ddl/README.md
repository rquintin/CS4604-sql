## Objective
Learn to manipulate data in the database and create database objects.

## Lab

### Setup
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/runme.sh && bash ./runme.sh 4`

You should see a bunch of output that ends in something like this:

```
COMMIT;
COMMIT
postgres=# create table basic_cards as select card_id, player_class, type, name, cost from cards limit 30;
SELECT 30
psql (10.1)
Type "help" for help.

postgres=#
```

A data set for the collection of cards for [Hearthstone](https://playhearthstone.com/en-us/),
the popular online card game by Blizzard. This data set is freely available from 
[Kaggle](https://www.kaggle.com/jeradrose/hearthstone-cards).

### Data Manipulation Language (DML)

#### Inserts

##### INSERT INTO

The INSERT INTO statement is used to insert new records in a table.

The syntax is:

```
INSERT INTO table_name (column1, column2, column3, ...)
VALUES (value1, value2, value3, ...);
```

Try this query:

```
INSERT INTO basic_cards (card_id, player_class, type, name, cost) VALUES ('CS4604', 'NINJA', 'HERO', 'Database Ninja', 20);
```

Note that PostgreSQL responds with `INSERT 0 1`. The '0' in that output is the Object Identifier (OID) and will always be zero for non-system tables. The '1' in the output is the number of rows inserted.

##### INSERT INTO SELECT

The INSERT INTO SELECT statement copies data from one table and inserts it into another table.

* INSERT INTO SELECT requires that data types in source and target tables match
* The existing records in the target table are unaffected

The syntax is:

```
INSERT INTO table2
SELECT * FROM table1
WHERE condition;
```

Alternately you can copy only some columns from one table into another table:

```
INSERT INTO table2 (column1, column2, column3, ...)
SELECT column1, column2, column3, ...
FROM table1
WHERE condition;
```


Try this query:

```
INSERT INTO basic_cards (card_id, player_class, type, name, cost)
SELECT card_id, player_class, type, name, cost 
FROM cards
WHERE cost > 10;
```

Notice that this query is selecting cards from the `cards` table and inserting those whose `cost > 10` into the basic_cards table.

* How many rows were inserted? 
* Do you recall how to query for those rows?

#### Updates

The UPDATE statement is used to modify the existing records in a table.

The syntax is:

```
UPDATE table_name
SET column1 = value1, column2 = value2, ...
WHERE condition;
```

**Note**: Be careful when updating records in a table! Notice the WHERE clause in the UPDATE statement. The WHERE clause specifies which record(s) should be updated. If you omit the WHERE clause, all records in the table will be updated!

Try this query:

```
UPDATE basic_cards SET cost = cost + 5 WHERE player_class = 'MAGE';
```

* How many rows were updated?

#### Deletes

The DELETE statement is used to delete existing records in a table.

The syntax is:

```
DELETE FROM table_name
WHERE condition;
```

**Note**: Be careful when deleting records in a table! Notice the WHERE clause in the DELETE statement. The WHERE clause specifies which record(s) that should be deleted. If you omit the WHERE clause, all records in the table will be deleted!

Try this query:

```
DELETE FROM basic_cards WHERE type = 'MINION';
```

### Data Definition Language (DDL)

#### Create table

The CREATE TABLE statement is used to create a new table in a database.

The syntax is:

```
CREATE TABLE table_name (
    column1 datatype,
    column2 datatype,
    column3 datatype,
   ....
);
```

Every database supports the expected set of datatypes (though they don't always call them the same thing). To view the list of datatypes available in PostgreSQL, click [here](https://www.postgresql.org/docs/9.5/static/datatype.html#DATATYPE-TABLE).

Try this query:

```
CREATE TABLE CS4604 (
  pid integer,
  name varchar(32),
  grade char(2)
);
```

You can also add defaults and constraints at creation time:

```
-- Drop the table created in the previous step
DROP TABLE CS4604;
-- Re-create it with constraints
CREATE TABLE CS4604 (
  pid integer PRIMARY KEY,
  name varchar(32) NOT NULL,
  grade varchar(2) NOT NULL DEFAULT 'A',
  CHECK (grade IN ('A', 'A-', 'B+', 'B', 'B-', 'C+', 'C-', 'D+', 'D', 'D-', 'F'))
);
```

In the above example we are creating the CS4604 table with a primary key of pid. Every collumn requires a value (NOT NULL). The default value for grade is 'A' and whatever value assigned to grade must be a valid letter grade.

Note that constraints for primary key and not null are "inline" with the column definition, but that the check constraint is after column definitions. Other constraints (ie unique) can also be specified after the column definition.

Also note that it is not necessary to drop and recreate a table to add constraints. Constraints can be added using the `alter table` command shown below.

#### Alter table

The ALTER TABLE statement is used to add, delete, or modify columns in an existing table.

The ALTER TABLE statement is also used to add and drop various constraints on an existing table.

The syntax is:

```
-- to add a column
ALTER TABLE table_name
ADD column_name datatype;

-- to drop a column
ALTER TABLE table_name
DROP COLUMN column_name;

-- to add a constraint
ALTER TABLE table_name
ADD constraint_clause;

-- to drop a constraint
ALTER table table_name
DROP constraint_clause;
```

Try this query:

```
ALTER TABLE CS4604
ADD CONSTRAINT ak1 UNIQUE (name);
```


#### Create index

The CREATE INDEX statement is used to create indexes in tables.

Indexes are used to retrieve data from the database very fast. The users cannot see the indexes, they are just used to speed up searches/queries.

**Note**: Updating a table with indexes takes more time than updating a table without (because the indexes also need an update). So, only create indexes on columns that will be frequently searched against.

The syntax is:

```
CREATE INDEX index_name
ON table_name (column1, column2, ...);
```

**Note**: Indexes are automatically created on primary key and unique key columns. The database uses the index to enforce the unique constraint.


Try this query:

```
CREATE INDEX IDX1_name
ON CS4604 (name);
```

* Describe the CS4604 table (`\d cs4604`). Notice that the description includes not only the table columns and datatypes, but also default values, indexes and constraints.

### Your turn

Now it's your turn to apply what you've learned. There is a larger table called **cards** in the database.
Use it to answer the questions in [Canvas](https://canvas.vt.edu/) for this lab.



