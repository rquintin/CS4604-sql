## Objective
Learn more advanced SQL queries.

## Lab

### Setup
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/runme.sh && bash ./runme.sh sql-practice`

### Queries

#### Determine the SQL necessary to answer the following queries from the `world` relation

* Show the population of France
* Show the name and population of Sweden, Norway and Denmark
* Show the names of countries where the population is between 200,000 and 250,000
* List each country name where the population is larger than that of 'Russia' (do this three different ways)
* Show countries in Europe with a per capita GDP greater than Italy
* List the name and continent of countries in the contienents containing either China or Italy
* Which country has a population that is more than Nepal, but less than Poland
* Which country has the greatest population for each continent?
* What is the total population of the world?
* List all the continents (just once each)
* What is the total GDP of Africa?
* For each continent show the continent and number of countries have a population of at least 10 million
* Which continents have a total population of at least 100 million

#### Determine the SQL necessary to answer the following queries from relations `agents`, `customers` and `orders`

* find the average purchase amount of all orders
* make a list with order no, purchase amount, customer name and their cities for those orders which order amount between 500 and 2000
* make a list of customer cities for which there are no agents in that area (do this 2 ways)
* how many customers are in each country?

## Attribution
This content from [SQLZoo](https://sqlzoo.net/) and [W3](https://www.w3resource.com/sql/sql-table.php)!
