## Objective
Learn how to use a basic SELECT statement.

## Lab

### Setup
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/runme.sh && bash ./runme.sh 1`

You should see a bunch of output that ends in something like this:

```
COMMIT;
COMMIT
create table basic_cards as select player_class, type, name, cost from cards limit 30;
SELECT 30
psql (10.1)
Type "help" for help.

postgres=#
```

A data set for the collection of cards for [Hearthstone](https://playhearthstone.com/en-us/),
the popular online card game by Blizzard. This data set is freely available from 
[Kaggle](https://www.kaggle.com/jeradrose/hearthstone-cards).

### SQL SELECT

The SELECT statement is used to select data from a database.
Basic syntax is 

```sql
SELECT column1, column2, ...
  FROM table_name;
```

Here, column1, column2, ... are the field names of the table you want to select data from. 
If you want to select all the fields available in the table, use the following syntax:

```sql
SELECT * FROM table_name;
```

The following SQL statement selects the "type" and "name" columns from the "basic_cards" table.
**Try executing this query in your docker playground.**

```sql
SELECT type, name FROM basic_cards;
```

The following SQL statement selects all the columns from the "basic_cards" table.
**Try executing this query in your docker playground.**


```sql
SELECT * FROM basic_cards;
```

### SELECT DISTINCT

The SELECT DISTINCT statement is used to return only distinct (different) values.
Inside a table, a column often contains many duplicate values; and sometimes you only want to list the different (distinct) values.
The SELECT DISTINCT statement is used to return only distinct (different) values.

The syntax is:

```sql
SELECT DISTINCT column1, colun2, ...
  FROM table_name;
```

**Try this query:**

```sql
SELECT DISTINCT type FROM basic_cards;
```

### WHERE Clause

The WHERE clause is used to filter records.
The WHERE clause is used to extract only those records that fulfill a specified condition.

The syntax is:

```sql
SELECT column1, column2, ...
  FROM table_name
 WHERE boolean_condition;
```

**Try this query:**

```sql
SELECT name FROM basic_cards WHERE type = 'HERO';
```

We can also use other types of comparisons (note that numeric comparisons do not use quotes):

```sql
SELECT name, cost FROM basic_cards WHERE cost > 5;
```

All of the standard operators are valid: =, <> (or !=), >, <, >=, <=.
These are additional (and very useful operators):

| Operator | Description | Example |
| -------- | ----------- | ------- |
| BETWEEN | Between an inclusive range | WHERE x BETWEEN 3 AND 9 |
| LIKE | Search for a pattern | WHERE x LIKE 'Smi%' |
| IN | To specify multiple possible values for a column | WHERE x IN (3,5,7,9) |

We can also apply boolean operators AND, OR and NOT.

AND syntax is:

```sql
SELECT column1, column2, ...
  FROM table_name
 WHERE condition1 AND condition2 AND condition3 ...;
```

OR syntax is:

```sql
SELECT column1, column2, ...
  FROM table_name
 WHERE condition1 OR condition2 OR condition3 ...;
```

NOT syntax is:

```sql
SELECT column1, column2, ...
  FROM table_name
 WHERE NOT condition;
```

As you would expect, you can combine the various logical operators to form complex boolean expressions.

**Try these queries:**

```sql
SELECT name, cost FROM basic_cards WHERE cost < 5 AND player_class = 'DRUID';
```

```sql
SELECT name, cost FROM basic_cards WHERE cost < 5 OR player_class = 'DRUID';
```

```sql
SELECT name, player_class, cost FROM basic_cards WHERE cost < 5 OR player_class IN ('DRUID', 'HUNTER');
```

### Your turn

Now it's your turn to apply what you've learned. There is a larger table called **cards** in the database.
Use it to answer the questions in [Canvas](https://canvas.vt.edu/) for this lab.
