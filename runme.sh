#!/bin/bash

MODULE="sql"
# Turn on debug
if [ "$1" == "-d" ]; then
  set -x
  shift
fi

# Cleanup in case this has previously been run
docker rm -f postgres >/dev/null 2>&1


# Startup server
clear
cat <<EOF




##########################################
#     Starting PostgreSQL Server         #
##########################################





EOF
docker run -d -e POSTGRES_HOST_AUTH_METHOD=trust --name postgres postgres






# While the server is starting up, clone the git repo
cat <<EOF




##########################################
#       Cloning Git Repository           #
##########################################





EOF
[ -d CS4604-${MODULE} ] && rm -rf CS4604-${MODULE}
git clone https://code.vt.edu/rquintin/CS4604-${MODULE}.git || exit 1

# Wait for server to initialize
while [ -z "$(docker logs postgres | grep 'database system is ready to accept connections')" ]; do
  sleep 1
done
docker logs postgres






# Startup client
cat <<EOF
##########################################
#     Connecting using psql client       #
##########################################
# Use Ctrl-D to exit
EOF
volume="$(pwd)/CS4604-${MODULE}:/sql"
if [ -n "$1" ]; then
  dir="$(ls -1d CS4604-${MODULE}/$1* 2>/dev/null |tail -1)"
  if [ -d "$dir" ]; then
    volume="$(pwd)/$dir:/sql"
    # Is there a startup shell script?
    if [ -f "$(pwd)/$dir/runme.sh" ]; then
      chmod a+rx $(pwd)/$dir/runme.sh
      $(pwd)/$dir/runme.sh
    fi
    # Is there a startup sql script?
    if [ -f "$(pwd)/$dir/runme.sql" ]; then
      startup="-f /sql/runme.sql"
      docker run -it --rm -v $(pwd)/logs:/logs -v $volume --link postgres:postgres postgres psql -a -h postgres -U postgres $startup
    fi
  fi
fi
if [ ! -f "$(pwd)/$dir/no-psql" ]; then
  docker run -it --rm -v $(pwd)/logs:/logs -v $volume --link postgres:postgres postgres psql -h postgres -U postgres 
  cat <<EOF



##########################################
# Server is still running. 
##########################################
# If you wish to stop the server, 
# run the following command:
# docker rm -f postgres

EOF
fi
