## Objective
Understand SQL Injection and how to prevent it.

## Lab

### Setup

#### Browser

For this lab, you will need to be able to edit form fields before submitting them.
This is a basic and common technique that attackers use to test websites for vulnerabilities.

We will use the [Chrome Browser](https://www.google.com/chrome/browser/desktop/index.html) for this lab. 
If you don't want to use Chrome, there are many alternatives. [Zap](https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project)
is an excellent choice. But setup and configuration of Zap (or other alternatives) are beyond the scope of this lab.


#### WebGoat
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/6.SQLi/runme.sh && bash ./runme.sh`

You should see a bunch of output that ends in something like this:

```
2017-11-21 16:14:34,998 INFO  - FrameworkServlet 'mvc-dispatcher': initialization completed in 314 ms
2017-11-21 16:14:35,018 INFO  - Initializing main webgoat servlet
2017-11-21 16:14:35,020 INFO  - Browse to http://localhost:8080/WebGoat and happy hacking!
Nov 21, 2017 4:14:35 PM org.apache.coyote.http11.Http11Protocol start
INFO: Starting ProtocolHandler ["http-bio-8080"]
```

At the top of the page, you should now see an IP address and the number **8080**.
That 8080 is a link, click on it. This should take you to a URL that looks something like:

http://ip172-18-0-22-b8a2ti5t4veg008spo30-8080.direct.labs.play-with-docker.com/

**Your exact URL will be different.**

You **should get an error** on this page. In your browser's address bar, append **/WebGoat/** to the end of the URL and hit enter.

You should now see something like this:

![webgoat](webgoat.png)

Login as the guest user.

### Numeric SQL Injection

* In the left-hand menu, click on **Injection Flaws**.
* In the left-hand menu, click on **Numeric SQL Injection**.
 * You should see a form that asks you to "Select your local weather station:"
* Click on the Go button
 * You should see wather results for Columbia weather station.
 * Note the SQL query used is also displayed. See that it is not using bind variables, but is simply concatenating the value for your selection onto the sql statement.

```sql
SELECT * FROM weather_data WHERE station = 101
```

* Right-click on the drop down box and select "Inspect"
 * This will open Developer Tools with the drop down element highlighted.
* Expand the Select and then double click on the first Option
 * Change the value to be **101 OR 1=1**
* Go back to the page in Chrome and click on the Go button

You should see:

**Bet you can't do it again! This lesson has detected your successful attack and has now switched to a defensive mode. Try again to attack a parameterized query.**

If you repeat the process, you should see that this hack will no longer work. This is because the query is now using a bind variable for the query.

```sql
SELECT * FROM weather_data WHERE station = ?
```

### String SQL Injection

* In the left-hand menu, click on **Injection Flaws**
* In the left-hand menu, click on **String SQL Injection**
 * You should see a form that asks you to "Enter your last name:"
* Enter into the box `a' OR 1=1 --`
* Click on the Go button

You should see a list of all users along with the message:

**Now that you have successfully performed an SQL injection, try the same type of attack on a parameterized query. Restart the lesson if you wish to return to the injectable query.**

### LAB: SQL Injection

Now that you've worked through the basic concepts, attempt the lab by completing each of the stages:

* Stage 1: String SQL Injection
* Stage 2: Parameterized Query #1
* Stage 3: Numeric SQL Injection
* Stage 4: Parameterized Query #2

For an extra challenge, attempt these:

* Database Backdoors
* Blind Numeric SQL Injection
* Blind String SQL Injection

When you're done, login to [Canvas](https://canvas.vt.edu/) to answer questions for this lab.



