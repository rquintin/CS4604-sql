## Objective
Understanding indexing

## Lab

### Setup
Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

`curl -o runme.sh https://code.vt.edu/rquintin/CS4604-sql/raw/master/runme.sh && bash ./runme.sh 5`

You should see a bunch of output that ends in something like this:

```
insert into big_cards select * from big_cards;
INSERT 0 1443328
psql (10.1)
Type "help" for help.

postgres=#
```

A data set for the collection of cards for [Hearthstone](https://playhearthstone.com/en-us/),
the popular online card game by Blizzard. This data set is freely available from 
[Kaggle](https://www.kaggle.com/jeradrose/hearthstone-cards).

### Indexing

Indexes are used to retrieve data from the database very fast. The users cannot see the indexes, they are just used to speed up searches/queries.

#### Explain Analyze 

In order to see the impact of our indexing efforts, we will use the `EXPLAIN ANALYZE` feature of Postgresql.

Example:

```
postgres=# EXPLAIN ANALYZE SELECT count(*) FROM big_cards;
                                           QUERY PLAN
--------------------------------------------------------------------------------------------------------
 Finalize Aggregate  (cost=77910.62..77910.63 rows=1 width=8) (actual time=6337.123..6337.124 rows=1 loops=1)
   ->  Gather  (cost=77910.40..77910.61 rows=2 width=8) (actual time=6337.093..6337.108 rows=3 loops=1)
         Workers Planned: 2
         Workers Launched: 2
         ->  Partial Aggregate  (cost=76910.40..76910.41 rows=1 width=8) (actual time=6311.393..6311.398 rows=1 loops=3)
               ->  Parallel Seq Scan on big_cards  (cost=0.00..73904.93 rows=1202192 width=0) (actual time=0.239..3459.826 rows=962219 loops=3)
 Planning time: 0.106 ms
 Execution time: 6356.337 ms
(8 rows)
```

**Note** that instead of returning the data, `EXPLAIN ANALYZE` provides a query plan detailing what approach the planner took to executing the statement provided along with the executiont time for the statement. These two pieces of information allow us to make performance tuning decisions about our query.

Most databases provide the ability to display "explain plans" or "query plans" used to execute queries. An explain plan is an ordered set of steps used by the RDBMS to access the requested data. 

Since SQL is declarative, there are typically a large number of alternative ways to execute a given query, with widely varying performance. When a query is submitted to the database, the query optimizer evaluates some of the different, correct possible plans for executing the query and returns what it considers the best option. Because query optimizers are imperfect, database users and administrators sometimes need to manually examine and tune the plans produced by the optimizer to get better performance. One way to to influence the plan is to create indexes. This creates additional possible (hopefully more performant) plans. However, because maintaining indexes can incur a performance penalty for updates, inserts and deletes we want to create the minimal set of indexes needed.

#### Using Indexes to improve performance

As a developer you know that your application will perform the query below *a lot*. It's not quite as fast as you would like it to be so you start performance tuning it.

**Query**: `select card_id, name from big_cards where race = 'TOTEM';`

You suspect an index will help, but before you make any changes you want to get a baseline explain plan and execution time. You do this using `EXPLAIN ANALYZE`.

**Execute**: `EXPLAIN ANALYZE SELECT card_id, name FROM big_cards WHERE race = 'TOTEM';`

```
postgres=# EXPLAIN ANALYZE SELECT card_id, name FROM big_cards WHERE race = 'TOTEM';
                                   QUERY PLAN
--------------------------------------------------------------------------------------------------------
 Gather  (cost=1000.00..78587.52 rows=6734 width=23) (actual time=1.099..261.358 rows=8192 loops=1)
   Workers Planned: 2
   Workers Launched: 2
   ->  Parallel Seq Scan on big_cards  (cost=0.00..76914.12 rows=2806 width=23) (actual time=0.501..255.621 rows=2731 loops=3)
         Filter: (race = 'TOTEM'::text)
         Rows Removed by Filter: 959488
 Planning time: 0.086 ms
 Execution time: 274.970 ms
```

You can see that the query take about 0.27seconds to execute and that it is doing a [parallel sequential scan](https://www.postgresql.org/docs/10/static/parallel-plans.html) on the big_cards table.

You suspect that an index on the race column will help. Let's create it.

**Execute**: `CREATE INDEX IDX1_big_cards ON big_cards(race);`

**Execute**: `EXPLAIN ANALYZE SELECT card_id, name FROM big_cards WHERE race = 'TOTEM';`

```
postgres=# EXPLAIN ANALYZE SELECT card_id, name FROM big_cards WHERE race = 'TOTEM';
                                   QUERY PLAN
--------------------------------------------------------------------------------------------------------
 Bitmap Heap Scan on big_cards  (cost=128.63..19610.19 rows=6736 width=23) (actual time=5.926..118.476 rows=8192 loops=1)
   Recheck Cond: (race = 'TOTEM'::text)
   Heap Blocks: exact=8192
   ->  Bitmap Index Scan on idx1_big_cards  (cost=0.00..126.95 rows=6736 width=0) (actual time=3.293..3.293 rows=8192 loops=1)
         Index Cond: (race = 'TOTEM'::text)
 Planning time: 0.310 ms
 Execution time: 128.241 ms
```

Congratulations! You have improved the query execution time by nearly 50%! Looking at the explain plan, you see that the query fetches index rows (Bitmap Index Scan) and uses those rows to fetch the card_id and name values from big_cards (Bitmap Heap Scan). 

Would it be possible to satisfy the query with an index only and further speed up the query?

**Execute**: `CREATE INDEX IDX2_big_cards ON big_cards(race, card_id, name);`

**Execute**: `EXPLAIN ANALYZE SELECT card_id, name FROM big_cards WHERE race = 'TOTEM';`

**Note**:
It's possible that your query, while perhaps faster, is still not being satisfied entirely by the index. The reason is that PostgreSQL maintains a [visibility map](https://www.postgresql.org/docs/9.5/static/storage-vm.html) of tuples for every relation. So even though all of the data that you need can be found in the index, the database still needs to check if each tuple is visible. You can read more about this [here](https://wiki.postgresql.org/wiki/Index-only_scans#What_types_of_queries_may_be_satisfied_by_an_index-only_scan.3F).

If you issue command `VACUUM big_cards;` and re-analyze you will likely see an explain plan that *is* satisfied by the index (and consequently much faster). However, subsequent updates to the table would cause this query to go back to the table to check the visibility map.

#### The performance cost of Indexes 

In general, we don't want to create unused indexes because they incur a performance penalty. The penalty is often minimal unless the application has a very high rate of updates. But it is something to be aware of.

Let's analyze an update of every row while our two indexes exist:

```
postgres=# explain analyze update big_cards set race = 'FOO';
                                       QUERY PLAN
--------------------------------------------------------------------------------------------------------
 Update on big_cards  (cost=0.00..187294.61 rows=5957661 width=348) (actual time=79175.854..79175.854 rows=0 loops=1)
   ->  Seq Scan on big_cards  (cost=0.00..187294.61 rows=5957661 width=348) (actual time=0.020..9261.931
 rows=2886656 loops=1)
 Planning time: 0.147 ms
 Execution time: 79175.926 ms
```

Note the Execution time.

Now let's drop the indexes and try again:

```
postgres=# drop index idx1_big_cards;
DROP INDEX
postgres=# drop index idx2_big_cards;
DROP INDEX
postgres=# explain analyze update big_cards set race = 'BAR';
                                       QUERY PLAN
--------------------------------------------------------------------------------------------------------
 Update on big_cards  (cost=0.00..279589.83 rows=8893483 width=348) (actual time=37555.290..37555.290 rows=0 loops=1)
   ->  Seq Scan on big_cards  (cost=0.00..279589.83 rows=8893483 width=348) (actual time=0.057..9477.515
 rows=2886656 loops=1)
 Planning time: 0.206 ms
 Execution time: 37555.362 ms
```

Notice that the update took half as long without the indexes. 

The moral of this lab is: create the indexes you need, but *only* the indexes you need.
